const map = require('../map')

const items = [1, 2, 3, 4, 5, 5];

function double(a){
    return a*2
}

const result1 = map(items, double)
console.log(result1)                        

const result2 = items.map(double)
console.log(result2)

const result3 = map(items, parseInt)
console.log(result3)

const result4 = items.map(parseInt)
console.log(result4)                        