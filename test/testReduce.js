const reduce = require('../reduce')

const items = [1, 2, 3, 4, 5, 5];

function accumulate (startingValue,currentValue){
    startingValue+=currentValue
    return startingValue
}

const result1 = reduce(items, accumulate )
const result2 = reduce(items, accumulate, 100)

console.log(result1,result2)