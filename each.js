function each(elements, cb) {
    if(Array.isArray(elements)){

        let result_array = []
        for (let i=0; i<elements.length; i++){
            result_array.push(cb(elements[i],i))
        }
        return result_array
    }else{
        return elements
    }
}

module.exports = each



