function filter(elements, cb) {
    
    let filtered_array =[]
    for (let index=0; index<elements.length; index++){
        if(cb(elements[index],index,elements) === true ){                    // when callback function satified it returns true
            filtered_array.push(elements[index])    
        }

    }
    return filtered_array

}
module.exports = filter
