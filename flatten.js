const reduce = require('./reduce')
const filter = require('./filter')

function temp (startingValue,currentValue){
    
    if(Array.isArray(currentValue)){
        for(let item of currentValue){
            if(Array.isArray(item)){
                startingValue = startingValue.concat([item])
            }else{
                startingValue = startingValue.concat(currentValue)
            }
        }
    }else{
        startingValue = startingValue.concat(currentValue)
    }
    return startingValue
}

function flatten(elements,depth=1) {


    let elements_without_holes = filter(elements,(each) => each !== undefined)
    elements = elements_without_holes
    // console.log(depth,elements)

    if(depth==0){           //base condition for recurssion
        return elements
    }else{
        let flattend_array = reduce(elements,temp,[])
        //console.log(depth,flattend_array)

        if(depth > 0){
            let completely_flattend = true          // to handle inifinity depth
            for(let item of flattend_array){
                if(Array.isArray(item)){
                    completely_flattend = false
                    break
                }
            }
    
            if(completely_flattend === false){
                flattend_array =flatten(flattend_array,depth-1)
            }else{
                flattend_array =flatten(flattend_array,0)
            }
    
            return flattend_array
        }
    }
    
}

module.exports = flatten